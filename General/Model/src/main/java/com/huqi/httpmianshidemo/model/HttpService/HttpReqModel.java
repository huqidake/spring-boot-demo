package com.huqi.httpmianshidemo.model.HttpService;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author 胡琦
 * @date 2021-07-24 星期六 15:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HttpReqModel {
    /**
     * 用户账户
     */
    private String userAccount;

    /**
     * 扣款金额
     */
    private BigDecimal cost;
}
