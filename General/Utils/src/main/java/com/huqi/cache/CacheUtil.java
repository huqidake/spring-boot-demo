package com.huqi.cache;

import com.huqi.date.DateUtil;
import com.huqi.json.JsonUtil;
import com.huqi.log.LoggerUtil;
import com.huqi.thread.ScheduledExecutorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 模拟redis操作, 懒得引入redis了
 * @author 胡琦
 * @date 2021-07-24 星期六 20:21
 */
public abstract class CacheUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(CacheUtil.class);

    /**
     * 默认放2000个吧
     */
    private static final int SIZE = 2000;
    private static final double RATIO = 0.95;

    /**
     * 存储主体
     */
    private static Map<String, Object> CACHE = new HashMap<>(SIZE);
    /**
     * 存放过期时间
     */
    private static Map<String, LocalDateTime> OVERDUE_TIME = new HashMap<>(SIZE);

    static {
        clearScheduled();
    }

    public static void init() {
        LoggerUtil.info(LOGGER, "CacheUtil init success");
    }

    /**
     * 存放一个
     * @param key
     * @return
     */
    public static boolean set(String key, Object value) {
        try {
            clearData();
            clearByKey(key);
            CACHE.put(key, value);
        }catch (Exception e) {
            LoggerUtil.error(e, LOGGER, "CacheUtil set error... key = {0} value = {1}"
                    , key, JsonUtil.toJson(value));
            return false;
        }
        return true;
    }

    /**
     * 存放一个带过期时间的key, 期间还有一些过期策略需要考虑
     * @param key
     * @param value
     * @param sec  过期时间, 秒
     * @return
     */
    public static boolean set(String key, Object value, long sec) {
        try {
            clearData();
            clearByKey(key);
            CACHE.put(key, value);
            OVERDUE_TIME.put(key, LocalDateTime.now().plusSeconds(sec));
            clearData();
        }catch (Exception e) {
            LoggerUtil.error(e, LOGGER, "CacheUtil set error... key = {0} value = {1}, sec = {2}"
                    , key, JsonUtil.toJson(value), String.valueOf(sec));
            return false;
        }
        return true;
    }

    /**
     * 取
     * @param key
     * @return
     */
    public static Object get(String key) {
        try {
            clearData();
            clearByKey(key);
            return CACHE.get(key);
        }catch (Exception e) {
            LoggerUtil.error(e, LOGGER, "CacheUtil get error... key = {0}", key);
            return null;
        }
    }

    /**
     * setnx操作
     * @param key
     * @param object
     * @return
     */
    public static boolean setNX(String key, Object object) {
        clearData();
        clearByKey(key);
        boolean ex = CACHE.containsKey(key);
        if (ex) {
            return false;
        }
        return set(key, object);
    }

    /**
     * setnx操作
     * @param key
     * @param object
     * @param sec
     * @return
     */
    public static boolean setNX(String key, Object object, long sec) {
        clearData();
        clearByKey(key);
        boolean ex = CACHE.containsKey(key);
        if (ex) {
            return false;
        }
        return set(key, object, sec);
    }

    /**
     * 删除数据
     * @param key
     * @return
     */
    public static boolean remove(String key) {
        try {
            clearData();
            CACHE.remove(key);
            clearByKey(key);
        }catch (Exception e) {
            LoggerUtil.error(e, LOGGER, "CacheUtil set remove... key = {0}", key);
            return false;
        }
        return true;
    }

    /**
     * 获取数据数目
     * @return
     */
    public static int keys() {
        clearData();
        return CACHE.size();
    }

    /**
     * 检查某个key是否已经过期,如果已经过期则进行删除
     * @param key
     */
    private static void clearByKey(String key) {
        // 查看key是否过期
        LocalDateTime time = OVERDUE_TIME.get(key);
        boolean exceeded = DateUtil.timeExceeded(time);
        if (exceeded) {
            OVERDUE_TIME.remove(key);
            CACHE.remove(key);
            LoggerUtil.info(LOGGER, "cache 清除了 key = {0}", key);
        }
    }

    /**
     * 检查所有的key,是否存在删除的,应该使用定时任务来进行处理,或者缓存过多时主动进行清除数据
     * ,单独启动一个线程进行操作,防止阻塞当前线程
     */
    private static void clear() {
        new Thread(() -> {
            Set<String> keySet = CACHE.keySet();
            if (!CollectionUtils.isEmpty(keySet)) {
                keySet.forEach(key -> clearByKey(key));
            }
        }).start();
    }

    /**
     * 当数据大于预设值的95成之后清除过期的key, 还可以写个算法清除最近过期的,随机算法清除, 丢弃最新值等算法,都行
     */
    private static void clearData() {
        if (CACHE.size() > SIZE * RATIO) {
            clear();
        }
    }

    /**
     * 随机取20个, 判断过期的数据,如果大于1/4的数据过期了就再重复此方法进行调用,算是一种递归调用
     * 主动进行删除过期的key, 随机取20个数据
     */
    private static void clearEliminate() {
        int num = 0;
        String[] strings = CACHE.keySet().toArray(new String[CACHE.size()]);
        if (strings.length == 0) {
            return;
        }
        for (int i = 0; i < 20; i++) {
            String key = strings[new SecureRandom().nextInt(strings.length)];
            boolean timeExceeded = DateUtil.timeExceeded(OVERDUE_TIME.get(key));
            if (timeExceeded) {
                OVERDUE_TIME.remove(key);
                CACHE.remove(key);
                LoggerUtil.info(LOGGER, "cache 清除了 key = {0}", key);
                num++;
            }
        }
        if (num >= 20/4) {
            clearEliminate();
        }
    }

    /**
     * 10分钟执行一次检查清除
     */
    private static void clearScheduled() {
        ScheduledExecutorUtil.getExecutorService().scheduleAtFixedRate(() -> {
            clearEliminate();
            clearData();
        }, 1, 1000, TimeUnit.MILLISECONDS);
    }
}
