package com.huqi.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

/**
 * 日志工具类
 * @author 胡琦
 * @date 2021-07-24 星期六 16:03
 */
public class LoggerUtil {
    private final static Logger LOGGER = LoggerFactory.getLogger(LoggerUtil.class);

    public static void info(Logger logger, String template, String... args) {
        if (logger.isInfoEnabled()) {
            logger.info(joint(template, args));
        }
    }

    public static void warn(Logger logger, String template, String... args) {
        logger.warn(joint(template, args));
    }

    public static void warn(Throwable throwable, Logger logger, String template, String... args) {
        logger.warn(joint(template, args), throwable);
    }

    public static void error(Logger logger, String template, String... args) {
        logger.error(joint(template, args));
    }

    public static void error(Throwable throwable, Logger logger, String template, String... args) {
        logger.error(joint(template, args), throwable);
    }

    /**
     * 解析占位符,返回拼接好的字符串
     *
     *
     * 我自己的实现方式
     *
     *     // {
     *     private static final char PREFIX = 123;
     *     // }
     *     private static final char SUFFIX = 125;
     *
     *     private static Set numSet = new HashSet(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
     *
     * private static String joint(String msg, String... msgs) {
     *         if (StringUtils.isBlank(msg) || msg == null || msgs.length == 0) {
     *             return msg;
     *         }
     *         final char[] chars = msg.toCharArray();
     *         StringBuilder stringBuilder = new StringBuilder();
     *         try {
     *             for (int i = 0; i < chars.length;) {
     *                 char c = chars[i];
     *                 if (c == PREFIX && chars[i+2] == SUFFIX) {
     *                     Integer index = Integer.valueOf(Character.toString(chars[i + 1]));
     *                     boolean contains = numSet.contains(index);
     *                     if (contains) {
     *                         stringBuilder.append(msgs[index]);
     *                     }
     *                     i = i+2;
     *                     continue;
     *                 }
     *                 if (c == SUFFIX) {
     *                     i++;
     *                     continue;
     *                 }
     *                 stringBuilder.append(c);
     *                 i++;
     *             }
     *         }catch (Exception e) {
     *             LOGGER.error("LoggerUtils joint error");
     *             return msg;
     *         }
     *         return stringBuilder.toString();
     *     }
     *
     *
     * @param msg  模板参数 示例
     *                          test1 = {0}, test2 = {1}
     *             填充的参数从0开始,数字和花括号之间不能有空格
     * @param args 需要填充的参数,
     * @return
     */
    private static String joint(String msg, String... args) {
        try {
            return MessageFormat.format(msg, args);
        } catch (Exception e) {
            LOGGER.error("日志打印异常", e);
        }
        return null;
    }

}