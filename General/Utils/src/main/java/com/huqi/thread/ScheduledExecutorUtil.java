package com.huqi.thread;

import com.huqi.log.LoggerUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @author 胡琦
 * @date 2021-07-29 星期四 22:56
 */
@Slf4j
public class ScheduledExecutorUtil {
    private ScheduledExecutorUtil() {}

    private final static ScheduledExecutorUtil scheduledExecutorUtil = new ScheduledExecutorUtil();

    private static final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(5);

    public static ScheduledExecutorUtil getInstance() {
        return scheduledExecutorUtil;
    }

    public static ScheduledExecutorService getExecutorService() {
        return executorService;
    }

    public void execute(Runnable runnable) {
        try {
            executorService.execute(runnable);
        }catch (Exception e) {
            LoggerUtil.warn(log, "ScheduledExecutorUtil execute warn!!");
        }
    }


}
