package com.huqi.date;

import com.huqi.log.LoggerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 时间工具类
 * @author 胡琦
 * @date 2021-07-24 星期六 15:59
 */
public class DateUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);

    /**
     * 默认日期解析格式
     */
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 获取当前时间字符串
     * @return
     */
    public static String getNowDateString() {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        final String date = dateFormat.format(new Date());
        LOGGER.info("DateUtil getNowDateString = " + date);
        return date;
    }

    /**
     * 转换指定的时间为标准格式
     * @param date
     * @return
     */
    public static String getDateString(Date date) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        String dateString = null;
        try {
           dateString = dateFormat.format(date);
        }catch (Exception e) {
            LoggerUtil.error(e, LOGGER, "DateUtil getDateString 转换异常");
            return null;
        }
        LOGGER.info("DateUtil dateString = " + dateString);
        return dateString;
    }

    /**
     * 获取指定日期的指定格式 String
     * @param date
     * @param formatString
     * @return
     */
    public static String getDateStringFormat(Date date, String formatString) {
        String dateString = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(formatString);
            dateString = dateFormat.format(date);
        }catch (Exception e) {
            LoggerUtil.error(e, LOGGER, "DateUtil getDateStringFormat 转换异常");
            return null;
        }
        LOGGER.info("DateUtil dateString = " + dateString);
        return dateString;
    }

    /**
     * 传入的时间是否大于当前时间, 即是还没有过期
     * @param time
     * @return
     */
    public static boolean timeExceeded(LocalDateTime time) {
        if (time == null)  {
            return false;
        }
        LocalDateTime now = LocalDateTime.now();
        return now.isAfter(time);
    }


}
