package com.huqi.httpmianshidemo.model.common.enums;

/**
 * @author 胡琦
 * @date 2021-07-24 星期六 22:52
 */
public enum ResultCode {

    SUCCESS(200, "成功请求"),
    FAIL(500, "服务器异常")


    ;

    /**
     * code码
     */
    private int code;

    /**
     * 描述
     */
    private String desc;

    ResultCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static ResultCode getResultByCode(int code) {
        for (ResultCode value : ResultCode.values()) {
            if (value.getCode() == code) {
                return value;
            }
        }
        return null;
    }
}
