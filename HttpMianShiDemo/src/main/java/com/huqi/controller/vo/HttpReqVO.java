package com.huqi.controller.vo;

import com.google.common.base.Preconditions;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * 请求入参
 * @author 胡琦
 * @date 2021-07-24 星期六 15:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HttpReqVO {

    /**
     * 用户账户
     */
    private String userAccount;

    /**
     * 扣款金额
     */
    private BigDecimal cost;

    /**
     * 幂等Key
     */
    private String requestKey;

    /**
     * 参数校验方法
     */
    public void check() {
        Preconditions.checkArgument(StringUtils.isNotBlank(userAccount), "userAccount 不能为空呢!");
        Preconditions.checkArgument(Objects.nonNull(cost), "cost 不能为空呢!");
        Preconditions.checkArgument(StringUtils.isNotBlank(requestKey), "requestKey 不能为空呢!");
    }

}
