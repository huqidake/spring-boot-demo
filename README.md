# SpringBootDemo

# 一道面试题

```
写一个银行系统的扣款HTTP接口，接口路径/bank/account/cost.json，入参为三个参数
//用户账户
String userAccount,
//扣款金额
BigDecimal cost，
//幂等Key
String requestKey。
数据库表名user_account，假设仅有id，userAccount(varchar 32)，amount(bigint)三个字段。
方法名任意，完成以下逻辑：
1.完成扣款逻辑，完成SpringMvc Controller、Manager、Mybatis Mapper三层伪代码。
2.接口仅能承载300QPS，请实现简单限流。
3.入参中有幂等key，请基于Nosql(Redis等) API实现简单幂等逻辑。
4.用自己习惯的日志框架API在你认为关键的位置添加日志内容。
5.在你认为需要抛出异常的地方，不要吝啬你的try catch，让我看到你的代码鲁棒性。
(以上实现代码需考虑系统并发)
附加：尽可能体现设计模式、封装多态等代码风格。
```