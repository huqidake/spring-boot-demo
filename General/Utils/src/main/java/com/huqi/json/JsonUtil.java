package com.huqi.json;


import com.alibaba.fastjson.JSON;
import com.huqi.log.LoggerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * json工具类
 * @author 胡琦
 * @date 2021-07-24 星期六 15:56
 */
public class JsonUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtil.class);

    /**
     * 使用fastJson转换对象为String
     * @param object
     * @return
     */
    public static String toJson(Object object) {
        if (object == null) {
            return null;
        }
        try {
         return JSON.toJSONString(object);
        }catch (Exception e) {
            LoggerUtil.error(e, LOGGER, "转换对象为Json格式失败");
            return null;
        }
    }

}
