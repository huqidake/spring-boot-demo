package com.huqi.servier.impl;

import com.huqi.httpmianshidemo.model.HttpService.HttpReqModel;
import com.huqi.json.JsonUtil;
import com.huqi.log.LoggerUtil;
import com.huqi.servier.HttpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author 胡琦
 * @date 2021-07-24 星期六 15:09
 */
@Service
public class HttpServiceImpl implements HttpService {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpServiceImpl.class);

    @Override
    public String deductions(HttpReqModel reqModel) {
        // 进行扣款的操作
        // 需要考虑线程安全的问题
        try {
            synchronized (reqModel.getUserAccount()) {
                kouKuan(reqModel);
            }
        }catch (Exception e) {
            LoggerUtil.error(e, LOGGER, "deductions 扣款失败了 req = {0}", JsonUtil.toJson(reqModel));
            return "扣款失败了啊";
        }
        return "扣款成功了呢";
    }

    @Transactional(rollbackFor = Exception.class)
    private void kouKuan(HttpReqModel reqModel) throws IllegalAccessException {
        // mapper扣款逻辑
        // 逻辑....
        // throw new IllegalAccessException("发生异常扣款失败了啊");
    }
}

