package com.huqi.result;

import com.huqi.httpmianshidemo.model.common.enums.ResultCode;
import com.huqi.httpmianshidemo.model.common.WebResult;

/**
 * 返回值工具类
 * @author 胡琦
 * @date 2021-07-24 星期六 22:55
 */
public class ResultUtil {

    public static WebResult success() {
        WebResult webResult = new WebResult();
        webResult.setSuccess(true);
        webResult.setData("ok");
        webResult.setCode(ResultCode.SUCCESS.getCode());
        return webResult;
    }

    public static <T> WebResult<T> success(T data) {
        WebResult<T> webResult = new WebResult();
        webResult.setSuccess(true);
        webResult.setCode(ResultCode.SUCCESS.getCode());
        webResult.setData(data);
        return webResult;
    }

    public static WebResult fail() {
        WebResult webResult = new WebResult();
        webResult.setSuccess(false);
        webResult.setCode(ResultCode.FAIL.getCode());
        webResult.setErrorMsg(ResultCode.FAIL.getDesc());
        return webResult;
    }

    public static WebResult fail(ResultCode resultCode) {
        WebResult webResult = new WebResult();
        webResult.setSuccess(false);
        webResult.setCode(resultCode.getCode());
        webResult.setErrorMsg(resultCode.getDesc());
        return webResult;
    }

    public static WebResult fail(String errorMsg) {
        WebResult webResult = new WebResult();
        webResult.setSuccess(false);
        webResult.setCode(ResultCode.FAIL.getCode());
        webResult.setErrorMsg(errorMsg);
        return webResult;
    }

    public static WebResult fail(ResultCode resultCode, String errorMsg) {
        WebResult webResult = new WebResult();
        webResult.setSuccess(false);
        webResult.setCode(resultCode.getCode());
        webResult.setErrorMsg(resultCode.getDesc() + errorMsg);
        return webResult;
    }

}
