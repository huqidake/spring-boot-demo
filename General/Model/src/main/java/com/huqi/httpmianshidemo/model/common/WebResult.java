package com.huqi.httpmianshidemo.model.common;

import lombok.Data;

/**
 * 返回值对象
 * @author 胡琦
 * @date 2021-07-24 星期六 22:52
 */
@Data
public class WebResult<T> {

    private int code;

    private T data;

    private String errorMsg;

    private boolean success;
}
