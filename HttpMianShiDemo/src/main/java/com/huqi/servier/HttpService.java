package com.huqi.servier;

import com.huqi.httpmianshidemo.model.HttpService.HttpReqModel;

/**
 * @author 胡琦
 * @date 2021-07-24 星期六 15:09
 */
public interface HttpService {

    /**
     * 扣款
     * @param reqModel
     * @return
     */
    String deductions(HttpReqModel reqModel);
}
